import System.IO
import System.Random
import Control.Monad (replicateM)

--main = replicateM 10 (randomIO :: IO Float) >>= print

main = do
  g <- newStdGen
  print . take 10 $ (randomRs (0, 9) g)

win :: IO ()
win = putStrLn "You win."

lose :: IO ()
lose = putStrLn "You lose."

askInput :: String -> IO String
askInput phrase = do 
    putStrLn phrase
    getLine

