import System.IO

main :: IO ()
main = do
    name <- getName
    greet name

getName :: IO String
getName = do 
    name <- askName
    case validateName name of
        Just error -> do 
            notAName error
            getName
        Nothing -> return name

validateName :: String -> Maybe String
validateName x  | x == "" = Just "is empty"
                | any (\x -> x == '\t') x = Just "contains a \\t"
                | length x < 2 = Just "is too short"
                | True =  Nothing

greet :: String -> IO ()
greet name = putStrLn ("Hello, " ++ name ++ ".")

askName :: IO String
askName = askInput "what is your name?"

notAName :: String -> IO ()
notAName reason = putStrLn ("Nice one, but that is not a name. I know it because it " ++ reason ++ " and that does not make a name. So...")

askInput :: String -> IO String
askInput phrase = do 
    putStrLn phrase
    getLine