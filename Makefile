########
# MAIN #
########

# DOC
#####

# Override
##########

# Const
#######
MAKEFILE_SOURCES = makefileSources

##########
# IMPORT #
##########
include $(MAKEFILE_SOURCES)/common.lib.mk
include $(MAKEFILE_SOURCES)/haskellProgram.lib.mk

#########
# RULES #
#########

.DEFAULT_GOAL: help

#############
# SHORTCUTS #
#############

.PHONY: dcu
dcu: docker-compose-up ## shortcut: build containers

.PHONY: dcub
dcub: build-bash ## shortcut: build containers and bash to it

.PHONY: dcd
dcd: docker-compose-down ## shortcut: stop containers and remove them

####################################
# SERVICE AND CONTAINER MANAGEMENT #
####################################

.PHONY: docker-compose-up
docker-compose-up: ## build the containers
	docker run -it --entrypoint=/bin/bash --volume=$(shell pwd):/home/project --workdir=/home/project -e http_proxy="${http_proxy}" -e https_proxy="${https_proxy}" -e no_proxy="${no_proxy}" haskell:latest 
#	docker-compose up -d --build

.PHONY: docker-compose-down
docker-compose-down: ## stop the containers and delete them
	docker-compose down --rmi local

#########
# BUILD #
#########

.PHONY: build-bash
build-bash: docker-compose-up ## open a bash stdin in the container able to run haskell
	docker-compose exec build /bin/bash

bin: ## create the bin directory for executable
	mkdir ./bin

.PHONY: bin-clean
bin-clean: ## clean the bin directory
	rm -f ./bin

/tmp/.cabal-updated:
	touch /tmp/.cabal-updated
	cabal -v update

cabal-update: /tmp/.cabal-updated

# Define rules for projects
$(eval $(call defineHaskellProgram,hello_world,hello_world.hs))
$(eval $(call defineHaskellProgram,what_is_your_name,main.hs))
$(eval $(call defineHaskellProgram,what_is_your_name_sugar,main.hs))

# Casino
#-------
.PHONY: casino
casino: casino-build  ##build the program

casino-re: casino-fclean bin/casino ##rebuild casino

casino-build: casino-dep bin/casino ## build the casino

casino-dep: /tmp/.casino-dep cabal-update ## get the casino deps

/tmp/.casino-dep:
	cabal install random
	touch /tmp/.casino-dep

bin/casino: bin
	ghc -o bin/casino casino/*.hs

bin/casino-clean:
	rm -f bin/casino

casino-clean:
	rm -f casino/*.hi
	rm -f casino/*.o

casino-fclean: casino-clean bin/casino-clean ##clean the program's generated files
