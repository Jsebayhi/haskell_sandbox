###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########

# Optional
##########

# Const
#######

##########
# IMPORT #
##########
#include $(MAKEFILE_SOURCES)/src.lib.mk

####################
# FUNCTIONNALITIES #
####################

#########
# RULES #
#########

.PHONY: help
# rules' name with length over 25 are truncated.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## show available rules with their description if exist
	@grep -h -E '^[0-9a-zA-Z_-]+:' $(MAKEFILE_LIST) | grep -v "### not shown" | awk 'BEGIN {FS = ":(.*?## )?"}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'
