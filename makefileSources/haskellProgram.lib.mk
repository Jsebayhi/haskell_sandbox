###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########

# Optional
##########

# Const
#######

##########
# IMPORT #
##########
#include $(MAKEFILE_SOURCES)/src.lib.mk

####################
# FUNCTIONNALITIES #
####################

## Usage:
## ----- $(eval $(call defineHaskellProgram, programExecDst, programSrc))
## Parameters:
## __________ $1: REQUIRED - Program executable destination
## __________ $2: REQUIRED - Program sources path
##
define defineHaskellProgram

$(1)-run: $(1) ##build then run the program's executable
	./$(1)

$(1)-re: $(1)-clean $(1) ##rebuild the program's executable

.PHONY: $(1)
$(1): bin/$(1)  ##build the program

bin/$(1): bin $(1)/.$(1)  ##build the program's executable
	#ghc -o bin/$(1) -tmpdir $(1)/.$(1) $(1)/*.hs
	ghc -o bin/$(1) $(1)/*.hs

bin/$(1)-clean:  ##clean the program's executable
	rm -f bin/$(1)

$(1)/.$(1):
	mkdir $(1)/.$(1)

$(1)/.$(1)-clean:
	rm -f $(1)/*.hi
	rm -f $(1)/*.o
	rm -rf $(1)/.$(1)

$(1)-clean: $(1)/.$(1)-clean bin/$(1)-clean ##clean the program's generated files

endef

#########
# RULES #
#########

help: defineHaskellProgramHelp

defineHaskellProgramHelp:
	@echo "(prgName)-run:            build then run the program's executable"
	@echo "(prgName)-re:             rebuild the program's executable"
	@echo "(prgName):                build the program"
	@echo "bin/(prgName):            build the program's executable"
	@echo "bin/(prgName)-clean :     clean the program's executable"
	@echo "(prgName)-clean:          clean the program's generated files"