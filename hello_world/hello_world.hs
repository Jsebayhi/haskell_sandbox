main :: IO ()
main = sayHello "toto"

sayHello :: String -> IO ()
sayHello x = putStrLn ("Hello, " ++ x ++ "!")