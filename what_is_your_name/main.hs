import System.IO

main :: IO ()
main = getName >>= greet

getName :: IO String
getName = askName >>= (\name ->
    case validateName name of
        Just error -> notAName error >> getName
        Nothing -> return name
    )

validateName :: String -> Maybe String
validateName x  | x == "" = Just "is empty"
                | any (\x -> x == '\t') x = Just "contains a \\t"
                | length x < 2 = Just "is too short"
                | True =  Nothing

greet :: String -> IO ()
greet name = putStrLn ("Hello, " ++ name ++ ".")

askName :: IO String
askName = askInput "what is your name?"

notAName :: String -> IO ()
notAName reason = putStrLn ("Nice one, but that is not a name. I know it because it " ++ reason ++ " and that does not make a name. So...")

askInput :: String -> IO String
askInput phrase = putStrLn phrase >> getLine

--return :: a -> IO a
--return a world0  =  (a, world0)